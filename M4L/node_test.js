const maxApi = require('max-api');

var data = require('./microwave_ii.json');
var params = {};
data.controlChangeCommands.forEach((param) => {
  params[param.name] = param.controlChangeNumber;
});

var device = data.device;

// maxApi.outlet({params: params, device: device});

maxApi.addHandler("bang", () => {
  maxApi.outlet({params: params, device: device});
});
