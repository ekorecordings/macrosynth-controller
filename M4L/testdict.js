autowatch = 1;
inlets = 2;
outlets = 5;

include("./lib.js");

log('in global code.');
var synthData;

var defaults = [5, 8, 13];

function bang() {
  log('testdict:bang');
  // this.patcher.getnamed('paramsListOne').message('set', 2);
  // this.patcher.getnamed('testUmenu').message('set', 1);

}

function dictionary(name) {
  log('testdict:dictionary');
  synthDict = new Dict(name);
  synthData = toObj(synthDict);
  log(synthData.device.name);

  // initialize the device and params data structures.
  var paramsDict = new Dict('paramsDict');
  outlet(0, 'clear');
  var params = _.map(synthData.controlChangeCommands, function (param) {
    paramsDict.set(param.controlChangeNumber, param.name);

    _.each(_.range(3), function(i) { // fill and set the three umenus
      outlet(i, 'append', fmtLabel(param.controlChangeNumber, param.name));
      outlet(i, 'set', defaults[i]);
    });


    return [param.controlChangeNumber, param.name];
  });

  var device = synthData.device;
  var deviceDict = new Dict('deviceDict');
  _.each(device, function(val, key) {
    deviceDict.set(key, val);
  });
}
