inlets = 2;
outlets = 2;
include("lib.js");

// initialize the dictionary objects
var synthDataDict = new Dict('synthDataDict'), synthData;
var paramsDict = new Dict('paramsDict'), paramsData;
var deviceDict = new Dict('deviceDict'), deviceData;
var defaultSettings = new Dict('defaultSettings'), defaultsData;
var currentSettings = new Dict('currentSettings'), currentData;

var events = new EventEmitter();

function bang() {
  log('init:bang', inlet);

  // copy data from the master syntData dictionary
  synthData = toObj(synthDataDict);

  // log(synthData.controlChangeCommands.length);

  // initialize the currentSettings dict from the defaults dict.
  // currentData = toObj(currentSettings);

  // log(JSON.stringify(currentData));

  var keys = currentSettings.getkeys();

  log(JSON.stringify(currentSettings.getkeys()));
  log(JSON.stringify(defaultSettings.getkeys()));

  if (keys === null) { // no current settings
    defaultsData = toObj(defaultSettings);
    _.each(defaultsData, function(item, key) {
      currentSettings.set(key, item);
    });
  }

  // copy the data from syntData into more convenient dict structures.
  _.each(synthData.controlChangeCommands, function(param) {
    paramsDict.set(param.name, param.controlChangeNumber);
  });

  _.each(synthData.device, function(value, name) {
    deviceDict.set(name, value);
  });
};
