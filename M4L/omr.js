const maxApi = require('max-api');
const https = require('https');
const MAX_ENV = process.env.MAX_ENV || false;

if (!MAX_ENV) {
  maxApi.post("No MAX_ENV environment variable, please run this script inside the Max runtime.");
  process.exit();
}

maxApi.addHandler("fetchdata", () => {
  fetchdata((e, r) => {
    if (e) throw err;
    maxApi.outlet(r);
  });
});

maxApi.addHandler("bang", () => {
  fetchdata((e, r) => {
    if (e) throw err;
    maxApi.outlet(r);
  });
});

function fetchdata(callback) {
  let testUrl = 'https://raw.githubusercontent.com/eokuwwy/open-midi-rtc-specs/master/specs/json/microwave_ii.json';
  getJSON(testUrl, (err, result) => {
    if (err) throw err;
    let params = {};

    result.controlChangeCommands.forEach((item) => {
      params[item.controlChangeNumber] = item.name;
    });

    callback(null, {params: params, device: result.device});
  });
}


// UTILS

let L = (data) => {
  if (arguments.length > 1) {
    data = [].slice.call(arguments);
  }
}

function getJSON(url, cb) {
  https.get(url, (resp) => {
    let data = '';
    resp.on('data', (chunk) => {
      data += chunk;
    });
    resp.on('end', () => {
      cb(null, JSON.parse(data));
    });
  }).on('error', (err) => {
    cb(err.message);
  });
}
