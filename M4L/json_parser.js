autowatch = 1;
inlets = 9;
outlets = 17;

include("./lib.js");
var synthDict = new Dict('synthDict'), synthData;
var paramsDict = new Dict('paramsDict'), paramsData;
var deviceDict = new Dict('deviceDict'), deviceData;
var defaultSettings = new Dict('defaultSettings'), defaultsData;
var currentSettings = new Dict('currentSettings'), currentData;

var events = new EventEmitter();

events.on('msg_int', function(args) {
  log('json_parser:msg_int', args);
});

function anything() {
  log('json_parser:anything', messagename);
  var args = [].slice.call(arguments);
  if (args.length === 0) { args = null; }
  events.emit(messagename, args);
}

// function bang() {
//   _.each(_.range(8), function(i) {
//     // var _i = initData[i];
//     // var label = fmtLabel(cc[_i], names[_i])
//     // outlet(i, parseInt(cc[_i]));
//     // outlet(i+8, 'set', label);
//     // outlet(inlet, defaultsData[i]);
//   });
// }

function msg_int(i) {
  var paramsData = toObj(paramsDict);
  var cc = _.values(paramsData);
  var names = _.keys(paramsData);
  // log('msg_int:actual', i, names[i]);
  var lbl = fmtLabel(cc[i], names[i]);
  // log(i, cc[i], names[i], );
  outlet(inlet, parseInt(cc[i]));
  outlet(inlet+8, '_parameter_shortname', lbl);

  // save the changed setting to currentSettings.
  var cur = toObj(currentSettings).eightKnobs;
  cur[inlet] = cc[i];
  currentSettings.set('eightKnobs', cur);
}
