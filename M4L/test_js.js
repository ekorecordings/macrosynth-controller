autowatch = 1;
inlets = 1;
outlets = 1;

// router for data from node.script object to different outlets
include("lib");

// --

function bang() {
  outlet(0, '_parameter_shortname', 'Test One');
}
