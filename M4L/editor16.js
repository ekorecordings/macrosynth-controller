autowatch = 1;
inlets = 2; // 0 = bang, 1 = ???
outlets = 9; // 8 = params to umenus, 0 - 7 = defaults / bang

include("lib.js");

//??? can I set umenu contents from code.
var synthDict = new Dict('synthDict'), synthData;
var paramsDict = new Dict('paramsDict'), paramsData;

var tmp = new Dict();

// paramsDict = tmp;

var deviceDict = new Dict('deviceDict'), deviceData;
var defaultSettings = new Dict('defaultSettings'), defaultsData;
var currentSettings = new Dict('currentSettings'), currentData;

var events = new EventEmitter();

// function anything() {
//   var args = [].slice.call(arguments);
//   if (args.length === 0) { args = null; }
//   log('anything', messagename);
//   events.emit(messagename, args);
// }
//
// events.on('clear', function() {
//   log('editor:clear');
//   _.each(_.range(8), function(o) {
//     outlet(o, 'clear');
//   });
// });
//
// events.on('reset', function() {
//   log('editor:reset');
//   var ccVals = _.values(paramsData);
//   defaultsData = toObj(defaultSettings);
//
//   _.each(_.range(8), function(o) {
//     var curCC = defaultsData.eightKnobs[o];
//     var curIndex = _.indexOf(ccVals, curCC);
//
//     // send the arr index of value curCC in paramsData
//     outlet(o, 'set', curIndex);
//     outlet(o, curIndex);
//     // send bang to get the value sent to the outlet of the live.numboxen in the parent patcher
//     outlet(o, 'bang');
//   });
//
// });
//
//
// events.on('bang', function() {
//   // initialize the dicts
//   log('editor:bang');
//
//   // we wait for the bang message create js objects from Dicts
//   synthData = toObj(synthDict);
//   paramsData = toObj(paramsDict);
//   deviceData = toObj(deviceDict);
//   currentData = toObj(currentSettings);
//
//   // tmp = paramsDict.export_json('./params-tmp.json');
//   var ccVals = _.values(paramsData);
//
//   // fill in all the params in the umenu lists
//   _.each(_.range(8), function(o) {
//     var curCC = currentData.eightKnobs[o];
//     var curIndex = _.indexOf(ccVals, curCC);
//
//     outlet(o, 'clear');
//     _.each(paramsData, function(cc, paramName) {
//       var lbl = fmtLabel(cc, paramName);
//       outlet(o, 'append', lbl);
//     });
//
//     // send the arr index of value curCC in paramsData
//     outlet(o, 'set', curIndex);
//     outlet(o, curIndex);
//     // send bang to get the value sent to the outlet of the live.numboxen in the parent patcher
//     outlet(o, 'bang');
//   });
// });
