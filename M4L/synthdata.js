inlets = 9;
outlets = 17;

include("lib");

var selectedParams;

var arrParams = [];

function p(args) {
  args = [].slice.call(arguments);
  post(args, "\n");
}

function s(a) {
  return JSON.stringify(a, false, '  ');
}


function dictionary(args) {
  var d = new Dict(args);
  selectedParams = JSON.parse(d.stringify());
  outlet(0, 'clear');
  p('cleared the list');

  arrParams.length = 0; // init length
  _.each(selectedParams, function(name, cc) {
    arrParams.push({name: name, cc: cc});
    outlet(0, 'append', name);
  });

  // p(s(arrParams));
}

function msg_int(arg) {
  p('int>', arg, s(arrParams[arg]));
  var param = arrParams[arg];
  // p(arrParams[arg].cc);
  p('inlet>', inlet); // XXX inlet is magical and tells us what port the message is coming from.
  outlet(inlet, parseInt(param.cc));
  outlet((inlet + 8), 'set', param.name);
}

// initialization

outlet(0, 'clear'); // clear the menu on start
